1. The code should compile with ```nvfortran -gpu=cc90 main.cuf```
2. ```./a.out``` should run on an H100...
3. with output analogous to the following: 
  ``` Found             1  device/s
  Device             0
  |  NVIDIA GeForce GTX 1080 Ti
  |           5375  MHz Mem Clock
  |      484.4400      GB/s Peak bandwidth
  |      10.90753      Gb Global Memory
  |   Compute capability            6 .            1
  Error:     0.000000
  ```
